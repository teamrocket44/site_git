<%--
  Created by IntelliJ IDEA.
  User: ACER-DUBOIS
  Date: 12/01/2018
  Time: 16:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="inc/style.css" />
    <link type="text/css" rel="stylesheet" href="inc/styleQCM.css" />
</head>
<body>
    <script>
        $('.message a').click(function(){
            $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
        });
    </script>
    <div class="login-page">
        <div class="form">
           <%-- <form class="register-form">
                <input type="text" id="nom" placeholder="name" />
                <input type="password" idplaceholder="password"/>
                <input type="text" placeholder="email address"/>
                <button>create</button>
                <p class="message">Already registered? <a href="#">Sign In</a></p>
            </form>--%>
            <form method="post" action="tableauDeBord" class="login-form">
                <input type="text" id="username" name="username" placeholder="username"/>
                <input type="password" id="mdp" name="password" placeholder="password"/>
                <button>login</button>
                <p class="message">Not registered? <a href="#">Create an account</a></p>
            </form>
        </div>
    </div>
</body>
</html>
