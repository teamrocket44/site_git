<%--
  Created by IntelliJ IDEA.
  User: ACER-DUBOIS
  Date: 26/02/2018
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" import="Model.*" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="inc/StyleQCM.css" />
    <title>Tableau bord</title>
</head>
<body>
<!-- En tete -->
<div class="header">
    <img class="homeBtn" src="image/home.png">
    <div class="TitrPage">
        <label><b><a href="#">Test 2017</a> > <a href="#">Formation RIL 2016</a> > <a href="#">test1</a></b></label>
    </div>
    <div class="TitrUser" >
        <label><b>Mr Jean Jacques</b></label>
    </div>
    <img class="logOBtn" src="image/logout.png">
</div>
<form>
    <div class="ContentQues">

        <div class="tritrQues">
            <label>Question N° ... </label>
            <img class="flag" src="image/flag_off.png" onclick="changeFlag()">
        </div>
        <div class="tpsQues"><label>Temps restant : ...</label></div>
        <div class="imgQues">
            <img class="ContImaQues" src="image/moi.jpg" onclick="showImg()">
        </div>
        <div class="Question">
            <p>
                Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.
            </p>
        </div>
        <div class='repQues'>
            <div class='ContentRep'>
                <div id="divRepA"><input type="checkbox" class="inpRep" name="salsifis" value="A" onclick="changeColor(this)"><label>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en </label></div>
                <div id="divRepB"><input type="checkbox" class="inpRep" name="salsifis" value="B" onclick="changeColor(this)"><label>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en </label></div>
                <div id="divRepC"><input type="radio" class="inpRep" name="salsifis" value="C" onclick="changeColor(this)"><label>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en </label></div>
                <div id="divRepD"><input type="radio" class="inpRep" name="salsifis" value="D" onclick="changeColor(this)"><label>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en</label> </div>
                <input type="hidden" id="inpFlag" name="inpFlag" value="">

            </div>
        </div>
        <div class="divBtn" align="right">
            <input type="submit" value="Question suivante" class="button">
        </div>
    </div>
</form>

<div id="grey" style="display:none" onclick="annule()">&nbsp;</div>
<div id="fixed" class="fixed" style="display:none">
    <img id="imgGiant" src="image/moi.jpg" onclick="annule()">
</div>

</body>
<script type="text/javascript">
    function changeColor(inp){
        num=inp.value;

        if(inp.checked==true){
            document.getElementById('divRep'+num).style.background='#05d0ea';
        }else{
            document.getElementById('divRep'+num).style.background='#969696';
        }
    }

    function showImg(){
        document.getElementById('grey').style.display='';
        document.getElementById('fixed').style.display='';
    }
    function annule(){
        document.getElementById('grey').style.display='none';
        document.getElementById('fixed').style.display='none';
    }

    function changeFlag(){
        var inp=document.getElementsByClassName("flag")[0];

        if((inp.src.indexOf("flag_off"))>0){
            inp.src="image/flag_on.png";
            document.getElementById('inpFlag').value='Marque';
        }else{
            inp.src="image/flag_off.png";
            document.getElementById('inpFlag').value='';
        }

    }
</script>