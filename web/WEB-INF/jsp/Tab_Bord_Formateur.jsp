<%@ page import="Model.Formateur" %>

<%--
  Created by IntelliJ IDEA.
  User: ACER-DUBOIS
  Date: 26/02/2018
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="Model.*" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <link rel="stylesheet" href="inc/styleQCM.css" />
    <title>Title</title>
</head>
<body>


<!-- En tete -->
<div class="header">
    <img class="homeBtn" src="inc/image/home.png">
    <div class="TitrPage">
        <label><b> Editer un test</b></label>
    </div>
    <div class="TitrUser" >
        <label><b>${formateur.nomFormateur}</b></label>
    </div>
    <img class="logOBtn" src="inc/image/logout.png">
</div>
<div class="Content">
    <!-- menu nav -->
    <div class='MenuNav'>
        <fieldset>
            <legend> Test : </legend>
            <div class="navigation">
                <ul>
                    <li class="has-sub"> <a href="#">Test 2016</a>
                        <ul>
                            <li> <a href="#">test 1</a></li>
                            <li><a href="#">test 2</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"> <a href="#">Test 2017 </a>
                        <ul>
                            <li> <a href="#">test 1</a></li>
                            <li><a href="#">test 2</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </fieldset>
    </div>


    <!-- créer test -->
    <div class="TabBord">
        <fieldset class="fieldCreaTest" style="height:530px;">
            <form method="post" action="creerTest">
            <legend>
                Créer un test.
            </legend>
            <label > Nom du test : </label>
            <br>
            <input type="text" name="nomTest" id="nomTest">
            <br>
            <label> Temps du test : </label>
            <br>
            <input type="number" name="tpsTest" id="tpsTest">


            <br>
            <div class="lstStag">
                <label class="sTitr"><u>Liste des stagiaires :</u></label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Jean Jacques</label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Benoit XVI</label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Roger Rabbit</label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Perceval</label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Carlos</label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Bob l'éponge</label>
                <br>
                <input type="checkbox" name="Stagi[]" value=""><label>Patrick</label>
                <br>
            </div>
            <div class="ContBtn">
                <input type="submit" class="button" value="Suivant">
            </div>
        </form>
        </fieldset>



    </div>

</div>




</body>
</html>
