<%@ page import="Model.Stagiaire" %>

<%--
  Created by IntelliJ IDEA.
  User: ACER-DUBOIS
  Date: 26/02/2018
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
import="Model.*" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="inc/style.css" />
    <title>Tableau bord</title>
</head>
<body>


<jsp:useBean id="stagiaire" class="Model.Stagiaire" scope="session" />

<!-- En tete -->
<div class="header">
    <img class="homeBtn" src="inc/image/home.png">
    <div class="TitrPage">
        <label><b>Tableau de bord</b></label>
    </div>
    <div class="TitrUser" >
       <%-- <label><b>${stagiaire.nomStagaire}</b></label>--%>
           <label><b>test</b></label>
    </div>
    <a href="tableauDeBord?deco=ok"> <img class="logOBtn" src="inc/image/logout.png"></a>

</div>
<div class="Content">
    <!-- menu nav -->
    <div class='MenuNav'>
        <fieldset>
            <legend> Test : </legend>
            <div class="navigation">
                <ul>
                    <li class="has-sub"> <a href="#">Test 2016</a>
                        <ul>
                            <li> <a href="#">test 1</a></li>
                            <li><a href="#">test 2</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"> <a href="#">Test 2017 </a>
                        <ul>
                            <li> <a href="#">test 1</a></li>
                            <li><a href="#">test 2</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </fieldset>
    </div>


    <!-- TAbleau de bord -->
    <div class="TabBord">
        <fieldset>
            <legend>
                Tableau de bord.
            </legend>
            <label class="NomTest"> Nom du test </label>
            <br>
            <label class="NomTest"> Temps : ( temps du test ) min</label>

            <div class="btnTst">
                <%--<input type='button' class="button" value="Passer le test">--%>
                <a href="question"><input type='button' class="button" value="Passer le test"></a>
            </div>
        </fieldset>

        <fieldset>
            <legend>
                Liste des sections.
            </legend>
            <div class="navigation">
                <ul>
                    <li class="has-sub"> <a href="#">Architecture</a>
                        <ul>
                            <li> <a href="#">Baroque</a></li>
                            <li><a href="#">Gothique</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"> <a href="#">Histoire </a>
                        <ul>
                            <li> <a href="#">Renaissance</a></li>
                            <li><a href="#">Moyen-age</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"> <a href="#">Math </a>
                        <ul>
                            <li> <a href="#">Algèbre</a></li>
                            <li><a href="#">Géométrie</a></li>
                        </ul>
                    </li>

                </ul>
            </div>


        </fieldset>

    </div>

</div>



</body>