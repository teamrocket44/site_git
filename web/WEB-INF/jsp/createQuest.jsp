<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="Model.Question" %>
<%--
  Created by IntelliJ IDEA.
  User: ACER-DUBOIS
  Date: 28/02/2018
  Time: 14:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Model.*" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="inc/styleQCM.css" />
    <title>Tableau bord</title>
</head>
<body>
<!-- En tete -->
<div class="header">
    <img class="homeBtn" src="inc/image/home.png">
    <div class="TitrPage">
        <label><b> Editer un test</b></label>
    </div>
    <div class="TitrUser" >
        <label><b>${formateur.nomFormateur}</b></label>
    </div>
    <img class="logOBtn" src="inc/image/logout.png">
</div>
<div class="Content">
    <!-- menu nav -->
    <div class='MenuNav'>
        <fieldset>
            <legend> Question : </legend>
            <div class="lstQues">

                <c:forEach items="${listeQuestion}" var="news" begin="0" end="9" varStatus="state">

                    <div><label> Question ${state.index} :</label>${news.libelleQuestion}<img src="inc/image/trash.png"></div>

                </c:forEach>
                <%--<div><label> Question 1 :</label>${Question.libelleQuestion}<img src="inc/image/trash.png"></div>
                <div><label> Question 2 :</label><img src="inc/image/trash.png"></div>
                <div><label> Question 3 :</label><img src="inc/image/trash.png"></div>
                <div><label> Question 4 :</label><img src="inc/image/trash.png"></div>
                <div><label> Question 5 :</label><img src="inc/image/trash.png"></div>--%>
                <div><label><i> Ajouter une question :</i></label><img src="inc/image/plusCircle.png"></div>
            </div>

        </fieldset>
    </div>


    <!-- créer test -->
    <div class="TabBord">
        <fieldset class="fieldCreaTest" style="height:530px;">
            <legend>
                Créer une question
            </legend>
            <form method="post"  action="insererQuestion">
                <textarea class="txtarQues" name="libelleQuestion" placeholder="Enoncé de la question"></textarea>

                <label>Joindre une image :</label>
                <input type="file" name="imgQues">


                <div class='ContentRep'>
                    <div id="divRepA"><input type="checkbox" class="inpRep" name="o_n_a" value="A" onclick="changeColor(this)"><textarea name="repQuesA"></textarea></div>
                    <div id="divRepB"><input type="checkbox" class="inpRep" name="o_n_b" value="B" onclick="changeColor(this)"><textarea name="repQuesB"></textarea></div>
                    <div id="divRepC"><input type="checkbox" class="inpRep" name="o_n_c" value="C" onclick="changeColor(this)"><textarea name="repQuesC"></textarea></div>
                    <div id="divRepD"><input type="checkbox" class="inpRep" name="o_n_d" value="D" onclick="changeColor(this)"><textarea name="repQuesD"></textarea></div>
                    <input type="hidden" id="inpFlag" name="inpFlag" value="">

                </div>

                <div class="divBtn" align="right">
                    <input type="submit" value="Enregister Question" class="button" style="width:150px;">
                </div>
            </form>
        </fieldset>



    </div>

</div>



</body>
<script type="text/javascript">
    function changeColor(inp){
        num=inp.value;

        if(inp.checked==true){
            document.getElementById('divRep'+num).style.background='#05d0ea';
        }else{
            document.getElementById('divRep'+num).style.background='#969696';
        }
    }

</script>
</html>
