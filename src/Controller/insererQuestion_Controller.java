package Controller;

import Model.Question;
import Model.Reponse;
import Model.Test;
import dao.QuestionDAO;
import dao.ReponseDAO;
import dao.TestDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/insererQuestion"})
public class insererQuestion_Controller extends HttpServlet {



    public static final String  LIBELLE_QUESTION   = "libelleQuestion";
    public static final String  ADRESSE_IMAGE   = "imgQues";
    public static final String  CHAMP_A   = "repQuesA";
    public static final String  CHAMP_B   = "repQuesB";
    public static final String  CHAMP_C   = "repQuesC";
    public static final String  CHAMP_D   = "repQuesD";
    public static final String checkboxA="o_n_a";
    public static final String checkboxB="o_n_b";
    public static final String checkboxC="o_n_c";
    public static final String checkboxD="o_n_d";

    public static final String VUE_CREATION_QUESTION = "/WEB-INF/jsp/createQuest.jsp";


    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {


        ServletContext context = getServletContext();
        RequestDispatcher disp = context.getRequestDispatcher(VUE_CREATION_QUESTION);
        disp.forward(request, response);

    }


    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {



        /* Question*/
        String libelleQuestion =request.getParameter(LIBELLE_QUESTION);
        String imgQuestion =request.getParameter(ADRESSE_IMAGE);

        QuestionDAO questionD = new QuestionDAO();
        Question question = new Question(libelleQuestion,imgQuestion,"checkbox",true);

        /*questionD.save(question);*/


        question.addReponse(new Reponse(request.getParameter(CHAMP_A),request.getParameter(checkboxA)=="A"));
        question.addReponse(new Reponse(request.getParameter(CHAMP_B),request.getParameter(checkboxB)=="B"));
        question.addReponse(new Reponse(request.getParameter(CHAMP_C),request.getParameter(checkboxC)=="C"));
        question.addReponse(new Reponse(request.getParameter(CHAMP_D),request.getParameter(checkboxD)=="D"));

        questionD.save(question);

        /*recuperer toute les questions*/
        List<Question> listeQuestion=questionD.findAll();
        HttpSession session = request.getSession(true);
        session.setAttribute("listeQuestion", listeQuestion);


        ServletContext context = getServletContext();
        RequestDispatcher disp = context.getRequestDispatcher(VUE_CREATION_QUESTION);
        disp.forward(request, response);


    }
}
