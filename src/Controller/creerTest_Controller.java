package Controller;

import Model.Test;
import dao.TestDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = {"/creerTest"})
public class creerTest_Controller extends HttpServlet {

    public static final String VUE_CREATION_QUESTION = "/WEB-INF/jsp/createQuest.jsp";
    public static final String CHAMP_NOM_TEST    = "nomTest";
    public static final String CHAMP_NOM_TPS    = "tpsTest";




    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        String nomTest =request.getParameter(CHAMP_NOM_TEST);
        String tpsTest =request.getParameter(CHAMP_NOM_TPS);




            int tps = Integer.parseInt(tpsTest);
            TestDAO testD = new TestDAO();
            Test test= new Test(nomTest,tps);
           // testD.save(test);


            ServletContext context = getServletContext();
            RequestDispatcher disp = context.getRequestDispatcher(VUE_CREATION_QUESTION);
            disp.forward(request, response);




    }
}
