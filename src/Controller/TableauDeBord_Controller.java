package Controller;

import Model.Formateur;
import dao.FormateurDAO;
import dao.StagiaireDAO;
import Model.Stagiaire;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet(urlPatterns = {"/tableauDeBord"})
public class TableauDeBord_Controller extends HttpServlet {

    public static final String ATT_USERNAME = "username";
    public static final String ATT_MDP = "password";

    public static final String VUE_SUCCES = "/WEB-INF/jsp/Tab_Bord.jsp";
    public static final String VUE_SUCCESF = "/WEB-INF/jsp/Tab_Bord_Formateur.jsp";
    public static final String VUE_CO = "/WEB-INF/jsp/Login.jsp";


    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        String login=request.getParameter( ATT_USERNAME);
        String password= request.getParameter( ATT_MDP );

      //  StagiaireDAO stagiaireD = new StagiaireDAO();

      /*  Stagiaire stagiaire = stagiaireD.auth(login, password);

        if(stagiaire != null)
        {
            HttpSession session = request.getSession(true);
            session.setAttribute("stagiaire", stagiaire);
            ServletContext context = getServletContext();
            RequestDispatcher disp = context.getRequestDispatcher( VUE_SUCCES);
            disp.forward(request, response);
        }*/


        FormateurDAO formateurD= new FormateurDAO();
        Formateur formateur = formateurD.auth(login,password);


        if(formateur != null)
        {
            HttpSession session = request.getSession(true);
            session.setAttribute("formateur", formateur);
            ServletContext context = getServletContext();
            RequestDispatcher disp = context.getRequestDispatcher( VUE_SUCCESF);
            disp.forward(request, response);

            StagiaireDAO stagiaireD=new StagiaireDAO();

            List<Stagiaire>stagiaire = stagiaireD.findAll();

            System.out.println(stagiaire.get(0));
            session.setAttribute( "listStag", stagiaire);
        }



      if (formateur == null)
        {
            response.sendRedirect(VUE_CO);
        }

    }



    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        String deco = request.getParameter("deco");

        if (deco.equals("ok")) {
            System.out.println("cool");
            HttpSession session = request.getSession();
            session.invalidate();

            /*redirection*/
            ServletContext context = getServletContext();
            RequestDispatcher disp = context.getRequestDispatcher(VUE_CO);
            disp.forward(request, response);

        }
    }
}
